class Grid():
    def __init__(self, rowCount, columnCount):
        self.rows = rowCount
        self.columns = columnCount
        self.states = []

    def getNode(self, row, column):
        return self[row][column]

    def walkMaze(self):
        startRow = self.rows - 1
        startCol = 0

        node = self.states[startRow][startCol]
        print "Starting in the bottom left at position: ("+str(startRow) + ", " + str(startCol) + ")"

        i = 0
        # while (node != self.states[0][self.columns -1]):
        while (i < 100):
            i += 1
        # for i in range(0, 887):
            node.printInf()
            if (node.direction == ">"):
                node = self.getEastNode(node)
            elif (node.direction == "v"):
                node = self.getSouthNode(node)
            elif (node.direction == "<"):
                node = self.getEastNode(node)
            elif (node.direction == "^"):
                node = self.getNorthNode(node)
            else:
                if (node.reward == 50):
                    print "Finished! at position (0, 9)"
                    return
                print "direction is " + str(node.direction) + ". Quitting"
                return

    def getNeighbors(self, node):
        neighbors = []


        north  = self.getNorth(node)
        east = self.getEast(node)
        south = self.getSouth(node)
        west = self.getWest(node)

    def rewardIterate(self, node):
        initialUtility = node.utility
        if (node.isApple() or node.isWall()):
            return
        northUtility = 0.8 * self.getNorthUtility(node) + 0.1 * self.getEastUtility(node) + 0.1 * self.getWestUtility(node)
        eastUtility = 0.8 * self.getEastUtility(node) + 0.1 * self.getNorthUtility(node) + 0.1 * self.getSouthUtility(node)
        southUtility = 0.8 * self.getSouthUtility(node) + 0.1 * self.getEastUtility(node) + 0.1 * self.getWestUtility(node)
        westUtility = 0.8 * self.getWestUtility(node) + 0.1 * self.getNorthUtility(node) + 0.1 * self.getSouthUtility(node)

        utilities = [northUtility, eastUtility, southUtility, westUtility]
        myMax = max(utilities)

        discount = 0.90
        node.utility = node.reward + (discount * float(myMax))
        node.setDirection(utilities.index(max(utilities)))
        return abs(node.utility - initialUtility)

    def getNorthUtility(self, node):
        if (node.row == 0):
            return 0
        else:
            return self.states[node.row-1][node.column].utility

    def getNorthNode(self, node):
        if (node.row == 0):
            return 0
        else:
            return self.states[node.row-1][node.column]

    def getEastUtility(self, node):
        if (node.column == self.columns - 1):
            return 0
        else:
            return self.states[node.row][node.column+1].utility

    def getEastNode(self, node):
        if (node.column == self.columns - 1):
            return 0
        else:
            return self.states[node.row][node.column+1]


    def getSouthUtility(self, node):
        if (node.row == self.rows - 1):
            return 0
        else:
            return self.states[node.row+1][node.column].utility

    def getWestUtility(self, node):
        if (node.column == 0):
            return 0
        else:
            return self.states[node.row][node.column-1].utility

    def getSouthNode(self, node):
        if (node.row == self.rows - 1):
            return 0
        else:
            return self.states[node.row+1][node.column]

    def getWestNode(self, node):
        if (node.column == 0):
            return 0
        else:
            return self.states[node.row][node.column-1]

class Node():
    def __init__(self, row, column, value, direction):
        self.row = row
        self.column = column
        self.value = value

        self.utility = 0.0

        #intialize rewards
        self.setDirection(direction)

        if (self.isWall() or not self.isOnGrid()):
            self.reward = 0.00 #Wall
        elif (self.isOpen()):
            self.reward = 0.00
        elif (self.isMountain()):
            self.reward = -1.0
        elif (self.isSnake()):
            self.reward = -2.0
        elif (self.isBarn()):
            self.reward = 1.0
        elif (self.isApple()):
            self.reward = 50
            self.utility = 50

    def setDirection(self, direction):
        if (direction == 0):
            self.direction = "^"
        elif (direction == 1):
            self.direction = ">"
        elif (direction == 2):
            self.direction = "v"
        elif (direction == 3):
            self.direction = "<"
        else:
            self.direction = "o"

    #If the type of space at location myTuple is the goal apple
    def isApple(self):
        return int(self.value) == 50

    def printInfo(self):
        print "\nrow: " + str(self.row)
        print "column: " + str(self.column)
        print "value: " + str(self.value)
        print "direction: " + str(self.direction)
        # print "directionValue: " + str(self.directionValue)

    def formalPrint(self):
        print str("%.1f" % self.utility) + " ",

    def directionPrint(self):
        print str(self.direction) + " ",

    def printReward(self):
        if (self.reward >= 0.0):
            print " " + str(self.reward) + " ",
        else:
            print str(self.reward) + " ",


    #If the type of space at location myTuple is open
    def isOpen(self):
        return int(self.value) == 0

    #If the type of space at location myTuple is a mountian
    def isMountain(self):
        return int(self.value) == 1

    #If the type of space at location myTuple is a wall
    def isWall(self):
        return int(self.value) == 2

    #If the type of space at location myTuple is a snake
    def isSnake(self):
        return int(self.value) == 3

    #If the type of space at location myTuple is a barn
    def isBarn(self):
        return int(self.value) == 4

    #If the type of space at location myTuple is the goal apple
    def isApple(self):
        return int(self.value) == 50.00

    def isOnGrid(self):
        return (self.row > -1) and (self.row < 8) and (self.column > -1) and (self.column < 10)

    def printInf(self):
        print "utility of " + str("%.3f" % self.utility) + " with direction " + str(self.direction)

