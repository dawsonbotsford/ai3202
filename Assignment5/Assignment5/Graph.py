import Queue
import math
from Grid import Grid
from Grid import Node

#Direction and help from http://www.redblobgames.com/pathfinding/a-star/implementation.html
class Graph():
    def __init__(self, mazeText, e):
        self.closedSet = {}

        self.rows = len(mazeText)
        self.columns = len(mazeText[0])
        print "\n\n\n"
        print "rows: " + str(self.rows)
        print "columns: " + str(self.columns)
        self.mazeText = mazeText
        self.e = e

        self.startNode = (self.rows - 1, 0)
        self.endNode = (0, self.columns - 1)

        self.nodeMap = []

        self.gamma = 0.9
        self.MDP()

    def printInfo(self):
        print 'rows: ' + str(self.rows)
        print 'columns: ' + str(self.columns)

    #If the type of space at location myTuple is open
    def isOpen(self, myTuple):
        return int(self.mazeText[myTuple[0]] [myTuple[1]]) == 0

    #If the type of space at location myTuple is a mountian
    def isMountain(self, myTuple):
        return int(self.mazeText[myTuple[0]] [myTuple[1]]) == 1

    #If the type of space at location myTuple is a wall
    def isWall(self, myTuple):
        return int(self.mazeText[myTuple[0]] [myTuple[1]]) == 2

    #If the type of space at location myTuple is a snake
    def isSnake(self, myTuple):
        return int(self.mazeText[myTuple[0]] [myTuple[1]]) == 3

    #If the type of space at location myTuple is a barn
    def isBarn(self, myTuple):
        return int(self.mazeText[myTuple[0]] [myTuple[1]]) == 4

    #If the type of space at location myTuple is the goal apple
    def isApple(self, myTuple):
        print "ISAPPLE CALLED\n"
        return int(self.mazeText[myTuple[0]] [myTuple[1]]) == 50

    def isOnGrid(self, myTuple):
        first, second = myTuple
        return (first > -1) and (first < self.rows) and (second > -1) and (second < self.columns)

    #Returns an array of all neighbors to inputted coord
    def neighbors(self, current):
        x, y = current
        return [(x-1, y), (x,y+1), (x+1, y), (x, y-1)]

    def rightNeighbor(self, indexOfCurrent, neighbors):
        rightIndex = indexOfCurrent + 1
        if (rightIndex >= len(neighbors)):
            return neighbors[0]
        else:
            return neighbors[rightIndex]

    def leftNeighbor(self, indexOfCurrent, neighbors):
        leftIndex = indexOfCurrent - 1
        if (leftIndex < 0):
            return neighbors[3]
        else:
            return neighbors[leftIndex]

    #Assuming the thing at the location myTuple is not a wall, return the reward of that piece
    #Assumes a living reward of zero
    def reward(self, myTuple):
        if (not self.isOnGrid(myTuple)):
            return -3 #Sufficiently large penalty so that we never move off grid
        elif (self.isMountain(myTuple)):
            return -1
        elif (self.isSnake(myTuple)):
            return -2
        elif (self.isBarn(myTuple)):
            return 1
        elif (self.isApple(myTuple)):
            return 50
        elif (self.isOpen(myTuple)):
            return 0
        else: #wall
            return -3

    #Return the index of the max utility in neighbors
    def utility(self, myTuple):
        neighbors = self.neighbors(myTuple)
        North = 0.8 * self.reward(neighbors[0]) + 0.1 * self.reward(self.rightNeighbor(0, neighbors)) + 0.1 * self.reward(self.leftNeighbor(0, neighbors))
        East = 0.8 * self.reward(neighbors[1]) + 0.1 * self.reward(self.rightNeighbor(1, neighbors)) + 0.1 * self.reward(self.leftNeighbor(1, neighbors))
        South = 0.8 * self.reward(neighbors[2]) + 0.1 * self.reward(self.rightNeighbor(2, neighbors)) + 0.1 * self.reward(self.leftNeighbor(2, neighbors))
        West = 0.8 * self.reward(neighbors[3]) + 0.1 * self.reward(self.rightNeighbor(3, neighbors)) + 0.1 * self.reward(self.leftNeighbor(3, neighbors))
        print "northUtil: " + str(North)
        print "eastUtil: " + str(East)
        print "southUtil: " + str(South)
        print "westUtil: " + str(West)
        utils = [North, East, South, West]
        print "myTuple: " + str(myTuple)
        # return self.reward(myTuple) + utils.index(max(utils))
        return self.reward(myTuple) + max(utils)

    #Prints the entire maze setup utilities
    def printAllUtilities(self, maze):
        print "\n"
        for row in maze:
            for columnn, item in enumerate(row):
                item.formalPrint()
            print "\n"

    #Prints the entire maze setup directions
    def printAllDirections(self, maze):
        print "\n"
        for row in maze:
            for columnn, item in enumerate(row):
                item.directionPrint()
            print "\n"

    def MDP(self):
        print "in MDP Successfully!"
        maze = []
        grid = Grid(self.rows, self.columns)

        #Create grid with Node class elements
        for row, a in enumerate(self.mazeText):
            myRow = []
            for column, value in enumerate(a):
                newNode = Node(row, column, value, None)
                myRow.append(newNode)

            maze.append(myRow)
            grid.states.append(myRow)

        maxChange = 99999
        while (maxChange >= float(self.e)*((1.0- 0.9)/0.9)): 
            utilChange = []
            for row in grid.states:
                for node in row:
                    utilChange.append(grid.rewardIterate(node))
            print "max utility change: " + str(max(utilChange))
            maxChange = max(utilChange)

        self.printAllDirections(maze)
        self.printAllUtilities(maze)
        grid.walkMaze()
