#!/usr/bin/env python
import sys
from Graph import Graph

"""
    input: file descriptor
    output: parsed input. An array of arrays
"""
def parseFile(fileDescriptor):
    parsed = []
    for line in fileDescriptor:
        parsed.append(line.rstrip('\n').split(' '))
    return parsed

if (len(sys.argv) < 2): #user did not input enough args
    print 'ERROR. Usage: python cli.py <World filename> <Value for e>'
    print 'Try: python ./cli.py World1.txt Manhattan'
    sys.exit()

if (len(sys.argv) == 2): #e was not passed in
    print "You didn't give me an \"e\" so I\'m using 0.5"
    e = 0.5
else: #e was passed in
    e = sys.argv[2]

fileName = sys.argv[1]

if (e < 0):
    print "e must be larger than zero"
    sys.exit()

if (fileName != "World1.txt"):
    print "We couldn't find that world, so we're using World1.txt for you\n"
    fileName = "World1.txt"

print 'Running MDP on Worlds/' + fileName + '.txt with an e value of ' + str(e)

#Open, read and parse
worldFile = open('Worlds/' + fileName, 'r')
parsedWorld = parseFile(worldFile)

#Convert to nodes and path find
Graph(parsedWorld, e)

#Close the file at the end
worldFile.close()

