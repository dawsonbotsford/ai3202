#Assignment 5
Markov Decision Process for map navigation in python

#Run
```shell
    ./cli.py <world filename> <value for e>
```

###Examples
```shell
    ./cli.py World1.txt 0.3
```

<br>
#Results
###Values for epsilon
In running the simulation with different values for ```0 < epsilon < 1``` I did **not** find any differences in the final path. I found this to be surprising, so I tried an epsilon value of 20 and **still got the same solution**. All of the full outputs are below.


###epsilon value of 0.0001
```
Running MDP on Worlds/World1.txt.txt with an e value of .0001

rows: 8
columns: 10
in MDP Successfully!
max utility change: 36.0
max utility change: 25.92
max utility change: 18.04866561
max utility change: 12.1963559295
max utility change: 8.66656821825
max utility change: 5.52280715101
max utility change: 3.91341072324
max utility change: 2.81306299856
max utility change: 2.02540535896
max utility change: 0.728468244559
max utility change: 0.210951484674
max utility change: 0.0526368237586
max utility change: 0.0121943690994
max utility change: 0.00296860521507
max utility change: 0.000688097172351
max utility change: 0.000154865825989
max utility change: 3.42293064648e-05
max utility change: 7.48061325062e-06


>  >  >  >  >  >  >  >  >  o

o  o  >  >  ^  ^  o  ^  o  ^

>  >  >  >  ^  ^  o  >  >  ^

o  ^  o  o  >  >  >  ^  o  ^

>  ^  o  >  ^  o  ^  ^  >  ^

^  ^  o  >  ^  o  >  ^  o  ^

^  <  o  ^  ^  o  ^  ^  o  o

^  >  >  >  >  >  ^  ^  <  <



3.0  4.2  5.9  7.6  9.7  13.9  18.2  26.6  36.0  50.0

0.0  0.0  4.4  6.2  8.4  8.7  0.0  19.2  0.0  36.0

2.5  3.5  4.4  5.6  7.0  4.9  0.0  17.4  20.0  27.7

0.0  3.5  0.0  0.0  5.7  6.5  11.2  14.5  0.0  20.0

2.1  2.7  0.0  3.4  4.4  0.0  8.0  10.1  10.0  15.3

1.5  0.1  0.0  3.0  3.4  0.0  6.9  7.9  0.0  11.0

1.0  -1.2  0.0  2.3  1.7  0.0  5.4  5.2  0.0  0.0

0.8  0.9  1.5  2.0  2.5  3.3  4.6  4.4  3.2  2.3

Starting in the bottom left at position: (7, 0)
utility of 0.793 with direction ^
utility of 0.983 with direction ^
utility of 1.516 with direction ^
utility of 2.093 with direction >
utility of 2.718 with direction ^
utility of 3.513 with direction ^
utility of 3.491 with direction >
utility of 4.409 with direction >
utility of 5.574 with direction >
utility of 6.967 with direction ^
utility of 8.364 with direction ^
utility of 9.747 with direction >
utility of 13.881 with direction >
utility of 18.186 with direction >
utility of 26.647 with direction >
utility of 36.000 with direction >
utility of 50.000 with direction o
Finished! at position (0, 9)
```


###epsilon value of 0.9999
```
Running MDP on Worlds/World1.txt.txt with an e value of .9999

rows: 8
columns: 10
in MDP Successfully!
max utility change: 36.0
max utility change: 25.92
max utility change: 18.04866561
max utility change: 12.1963559295
max utility change: 8.66656821825
max utility change: 5.52280715101
max utility change: 3.91341072324
max utility change: 2.81306299856
max utility change: 2.02540535896
max utility change: 0.728468244559
max utility change: 0.210951484674
max utility change: 0.0526368237586


>  >  >  >  >  >  >  >  >  o

o  o  >  >  ^  ^  o  ^  o  ^

>  >  >  >  ^  ^  o  >  >  ^

o  ^  o  o  >  >  >  ^  o  ^

>  ^  o  >  ^  o  ^  ^  >  ^

^  ^  o  >  ^  o  >  ^  o  ^

^  <  o  ^  ^  o  ^  ^  o  o

^  >  >  >  >  >  ^  ^  <  <



3.0  4.2  5.8  7.6  9.7  13.9  18.2  26.6  36.0  50.0

0.0  0.0  4.4  6.2  8.4  8.7  0.0  19.2  0.0  36.0

2.5  3.5  4.4  5.6  7.0  4.9  0.0  17.4  20.0  27.7

0.0  3.5  0.0  0.0  5.7  6.5  11.2  14.5  0.0  20.0

2.1  2.7  0.0  3.4  4.4  0.0  8.0  10.1  10.0  15.3

1.5  0.1  0.0  3.0  3.4  0.0  6.9  7.9  0.0  11.0

1.0  -1.2  0.0  2.3  1.7  0.0  5.4  5.2  0.0  0.0

0.8  0.9  1.5  2.0  2.5  3.3  4.6  4.4  3.2  2.3

Starting in the bottom left at position: (7, 0)
utility of 0.782 with direction ^
utility of 0.970 with direction ^
utility of 1.503 with direction ^
utility of 2.077 with direction >
utility of 2.714 with direction ^
utility of 3.510 with direction ^
utility of 3.486 with direction >
utility of 4.408 with direction >
utility of 5.574 with direction >
utility of 6.967 with direction ^
utility of 8.363 with direction ^
utility of 9.747 with direction >
utility of 13.881 with direction >
utility of 18.186 with direction >
utility of 26.647 with direction >
utility of 36.000 with direction >
utility of 50.000 with direction o
Finished! at position (0, 9)
```

###epsilon value of 20
```
Running MDP on Worlds/World1.txt.txt with an e value of 20

rows: 8
columns: 10
in MDP Successfully!
max utility change: 36.0
max utility change: 25.92
max utility change: 18.04866561
max utility change: 12.1963559295
max utility change: 8.66656821825
max utility change: 5.52280715101
max utility change: 3.91341072324
max utility change: 2.81306299856
max utility change: 2.02540535896


>  >  >  >  >  >  >  >  >  o

o  o  >  >  ^  ^  o  ^  o  ^

>  >  >  >  ^  ^  o  >  >  ^

o  ^  o  o  >  >  >  ^  o  ^

>  ^  o  >  ^  o  ^  ^  >  ^

^  ^  o  >  ^  o  >  ^  o  ^

^  <  o  ^  ^  o  ^  ^  o  o

^  >  >  >  >  >  ^  ^  <  <



2.0  3.8  5.7  7.6  9.7  13.9  18.2  26.6  36.0  50.0

0.0  0.0  4.2  6.2  8.4  8.7  0.0  19.2  0.0  36.0

1.6  3.1  4.3  5.6  7.0  4.9  0.0  17.4  20.0  27.7

0.0  3.3  0.0  0.0  5.7  6.5  11.2  14.5  0.0  20.0

1.5  2.5  0.0  3.4  4.4  0.0  8.0  10.1  10.0  15.3

1.1  -0.1  0.0  2.9  3.4  0.0  6.9  7.9  0.0  11.0

0.6  -1.5  0.0  2.3  1.7  0.0  5.4  5.2  0.0  0.0

0.5  0.7  1.4  2.0  2.5  3.3  4.6  4.4  3.2  2.3

Starting in the bottom left at position: (7, 0)
utility of 0.481 with direction ^
utility of 0.625 with direction ^
utility of 1.075 with direction ^
utility of 1.549 with direction >
utility of 2.481 with direction ^
utility of 3.252 with direction ^
utility of 3.128 with direction >
utility of 4.318 with direction >
utility of 5.551 with direction >
utility of 6.960 with direction ^
utility of 8.358 with direction ^
utility of 9.744 with direction >
utility of 13.881 with direction >
utility of 18.186 with direction >
utility of 26.647 with direction >
utility of 36.000 with direction >
utility of 50.000 with direction o
Finished! at position (0, 9)
```

<br>
<br>
#What is this?
```open Assignment5.pdf``` if the summary below is not enough

###Movement
* Discount factor of 0.9
* Mountains (**1**) have a negative reward of -1
* Snakes (**3**) have a negative reward of - 2
* Barn (**4**) has a positive reward of 1
* The goal state reward is 50 (top right of the world)
* Offgrid and wall movements have an arbitrary penalty of -3
* The horse is successful 80% of the time
  * 10% of the time he will go left of the intended space
  * 10% of the time he will go right of the intended space
* You cannot move through or make use of walls (**2**)

###Number basics
* **0**: Open Square
* **1**: Mountain Square
* **2**: Wall
* **3**: Snake
* **4**: Barn

