import Queue
import math

#Direction and help from http://www.redblobgames.com/pathfinding/a-star/implementation.html
class Graph():
    def __init__(self, mazeText, heuristic):
        self.closedSet = {}
        self.rows = len(mazeText)
        self.columns = len(mazeText[0])
        self.mazeText = mazeText

        self.startNode = (self.rows - 1, 0)
        self.endNode = (0, self.columns - 1)

        #self.buildNodes()
        self.aStar(heuristic)

    def printInfo(self):
        print 'rows: ' + str(self.rows)
        print 'columns: ' + str(self.columns)


    """Takes in two positions and calculates the cost to move"""
    def manhattan(self, current, next):
        return 10 * (abs(current[0] - next[0]) + abs(current[1] - next[1]))

    def euclidian(self, current, next):
        return 10 * math.sqrt(math.pow(current[1] - next[1], 2) + math.pow(current[0] - next[0], 2))

    def isMountain(self, myTuple):
        return int(self.mazeText[myTuple[0]] [myTuple[1]]) == 1

    def isWall(self, myTuple):
        return int(self.mazeText[myTuple[0]] [myTuple[1]]) == 2

    #Returns an array of all neighbors to inputted coord
    def neighbors(self, current):
        def isValid(myTuple):
            first, second = myTuple
            return (first > -1) and (first < self.rows) and (second > -1) and (second < self.columns)


        def validNeighbors(neighborNodes):
            toReturn = []
            for elem in neighborNodes:
                if isValid(elem) and not self.isWall(elem):
                    toReturn.append(elem)
            return toReturn

        x, y = current

        #Brute force neighbor add
        neighborNodes = [(x-1, y), (x-1, y+1), (x,y+1), (x+1,y+1), (x+1, y), (x+1, y-1), (x, y-1), (x-1, y-1)]

        #Return only valid neighbors
        return validNeighbors(neighborNodes)


    def cost(self, curr, next):
        if (int(self.manhattan(curr, next)) == 10):

            toReturn = 10
        else:
            toReturn = 14
        if (self.isMountain(next)):
            toReturn += 10
        return toReturn

    def aStar(self, heuristic):
        def printPath(curr):
            toPrint ="Starting from the end: "
            while (not curr == None):
                toPrint += str(curr) + ", "
                curr = cameFrom[curr]
            print toPrint

        que = Queue.PriorityQueue()
        que.put((0, self.startNode))
        locationsEvaluated = 0
        cameFrom = {self.startNode: None}

        # Best cost possible to node. Schema: {(x1, y1): costTo1, (x2, y2), costTo2}
        # Also used to aggregate visited nodes
        costSoFar = {self.startNode: 0}

        while (not que.empty()):
            (garbage, curr) = que.get()
            if (curr == self.endNode):
                print "------ path found --------"
                print "cost: " + str(costSoFar[self.endNode])
                print "locations evaluated: " + str(locationsEvaluated)
                printPath(self.endNode)
                break

            for nextNode in self.neighbors(curr):
                locationsEvaluated += 1
                newCost = costSoFar[curr] + self.cost(curr, nextNode)

                #if never visited or a faster route to node is found
                if nextNode not in costSoFar or newCost < costSoFar[nextNode]:
                    costSoFar[nextNode] = newCost
                    if (heuristic == 'manhattan'):
                        optimality = newCost + self.manhattan(nextNode, self.endNode)
                    else:
                        optimality = newCost + self.euclidian(nextNode, self.endNode)
                    que.put((optimality, nextNode))
                    cameFrom[nextNode] = curr

