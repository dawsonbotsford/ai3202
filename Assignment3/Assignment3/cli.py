#!/usr/bin/env python
import sys
from Graph import Graph

"""
    input: file descriptor
    output: parsed input. An array of arrays
"""
def parseFile(fileDescriptor):
    parsed = []
    for line in fileDescriptor:
        parsed.append(line.rstrip('\n').split(' '))
    return parsed

if (len(sys.argv) < 3):
        print 'ERROR. Usage: python cli.py <World filename> <Heuristic>'
        print 'Try: python ../cli.py World1.txt Manhattan'
        sys.exit()

fileName = sys.argv[1]
heuristic = sys.argv[2]

if (heuristic != 'manhattan'):
    heuristic = 'euclidian'

print 'Running ' + heuristic + ' A* on Worlds/World' + fileName + '.txt'

#Open, read and parse
worldFile = open('Worlds/World' + fileName + '.txt', 'r')
parsedWorld = parseFile(worldFile)

#Convert to nodes and path find
Graph(parsedWorld, heuristic)

#Close the file at the end
worldFile.close()

