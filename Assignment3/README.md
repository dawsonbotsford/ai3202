#Assignment 3
A* Path finding algorithms in python

#Run
```shell
    ./cli.py <worldNumber> <heuristic>
```

###Examples
```shell
    ./cli.py 1 manhattan
    ./cli.py 2 manhattan
    ./cli.py 1 euclidian
    ./cli.py 2 euclidian
```

<br>
#Results
####Format
The locations are listed as indexes into a two-dimensional array. In other words:
    
    Starting position (bottom left): (7,0)
    Ending position (top right): (0,9)


The results are listed from *the end position to the beginning*

<br>
##World 1
####Manhattan 
    Running manhattan A* on Worlds/World1.txt
    ------ path found --------
    cost: 156
    locations evaluated: 103
    Starting from the end: (0, 9), (0, 8), (0, 7), (0, 6), (0, 5), (1, 4), (2, 3), (2, 2), (3, 1), (4, 1), (5, 1), (6, 1), (7, 0)

####Euclidian
  
    Running euclidian A* on Worlds/World1.txt
    ------ path found --------
    cost: 130
    locations evaluated: 131
    Starting from the end: (0, 9), (0, 8), (1, 7), (2, 7), (3, 6), (3, 5), (4, 4), (5, 4), (6, 3), (7, 2), (7, 1), (7, 0),

<br>
<br>
##World 2
####Manhattan 

    Running manhattan A* on Worlds/World2.txt
    ------ path found --------
    cost: 142
    locations evaluated: 57
    Starting from the end: (0, 9), (0, 8), (0, 7), (0, 6), (0, 5), (1, 4), (2, 4), (3, 4), (4, 3), (4, 2), (4, 1), (5, 0), (6, 0), (7, 0)

####Euclidian

    Running euclidian A* on Worlds/World2.txt
    ------ path found --------
    cost: 142
    locations evaluated: 162
    Starting from the end: (0, 9), (0, 8), (0, 7), (0, 6), (0, 5), (1, 4), (2, 4), (3, 4), (4, 4), (5, 3), (6, 3), (7, 2), (7, 1), (7, 0)

<br>
<br>
##Manhattan vs. Euclidian Heuritic
As an optional heuristic I chose euclidian which is represented with the pythag formula: 

####Equation:
```python
10 * math.sqrt(math.pow(current[1] - next[1], 2) + math.pow(current[0] - next[0], 2))
```
<br>
####Motivation: 
The manhattan distance is quick to give a distance estimation, but euclidian is a **straight-line distance**. We've been talking about straight-line distances as a good heuristic from the start of the class, so it only made sense to implement this.

<br>
####Performance and Analyis: 
The exact metrics for the euclidian performance are already listed above ^. But as far as a verbal explanation:

<br>
**euclidian performed much better on map 1**. The cost of euclidian was 130 while manhattan was 156. And this much lower cost came at only a marginally larger amount of calculations. euclidian evaluated 131 locations while manhattan evaluated 103.

**euclidian performed slightly worse on map 2**. The cost is the exact same between the two heurisics, yet euclidian had to evaluate 162 locations in comparison to manhattan's low 57 locations.

<br>
<br>
#What is this?
```open Assignment3.pdf``` if the summary below is not enough

###Movement
* Standard Horizontal/Vertical: **10** cost
* Standard Diagonal: **14** cost
* Add a cost of 10 if you are moving onto a mountain (**1**)
* You cannot move through or make use of walls (**2**)

###Number basics
* **0**: Open Square
* **1**: Mountain Square
* **2**: Wall

