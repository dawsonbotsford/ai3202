#Assignment6
Simplistic. Try the following commands

##Test runner
```shell
./testAll.sh
```
<br>
<br>
####Here's what the test runner runs
```
//marginal examples
python cli.py -mD //0.304 just like the doc!
python cli.py -mS //0.3 just like the doc!
python cli.py -mC //0.011 just like the doc!

//conditional examples
python cli.py -g"C|S" //0.032 just like the doc!
python cli.py -g"d|S" //0.311 just like the doc!
python cli.py -g"C|C" //1 just like the doc!

//joint examples
python cli.py -jPSC //Huge output!
python cli.py -jpsc //Calculate joint
python cli.py -j~p~s~c //Calculate joint
```
