#! /usr/bin/python
# Dawson Botsford 11/12/2015

#Input has two columns - state & output
letters = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','_']


#P(X)
#This means that we only care about the first column
def marginal(file):
    print '----------start marginal-------------------'

    #Initialize the stateCount to zero
    stateCount = {}
    for letter in letters:
        stateCount[letter] = 0
    f = open(file, 'r')
    for line in f:
        stateCount[line[0]] += 1

    sum = 0
    for key, value in stateCount.iteritems():
        prob = float(value) / 160980
        sum += prob
        print 'P[' + key + '] = ' + str(prob)
    # print 'total sum: ' + str(sum)
    print '----------end marginal-------------------'


#sums the amount of occurances of character in the left column (X)
def stateSum(fileName, character):
    f = open(fileName, 'r')
    sum = 0
    for line in f:
        if (line[0] == character):
            sum += 1
    return sum

#P(E | X)
#Here we care about both columns
def emission(file):
    print '----------start emission-------------------\n'

    #Create nested dictionaries for each character to map to each possible character
    states = {}
    for stateLetter in letters:
        evidence = {}
        for evidenceLetter in letters:
            #Initialize the stateCount to one
            #This is the numerator's part for smoothing
            evidence[evidenceLetter] = 1
        states[stateLetter] = evidence

    f = open(file, 'r')

    for line in f:
        states[line[0]][line[2]] += 1

    #Run 27 times
    for stateKey, stateValue in states.iteritems():
        print 'P(Et | Xt)'

        #Run 27 times
        sumOfState = 0
        for evidenceKey, evidenceValue in stateValue.iteritems():
            prob = float(float(evidenceValue) / float((27 + stateSum(file, stateKey))))
            sumOfState += prob
            print 'P( E[' + evidenceKey + '] | X[' + stateKey + '] = ' + str(prob)

        #Always equals 1
        print 'Total probability of any evidence given state \'' + str(stateKey) + '\': '+ str(sumOfState) + '\n'
    print '----------end emission-------------------\n'

#P(X[t + 1] | X[t])
#This means that we only care about the first column
def transition(file):
    print '----------start transition-------------------\n'

    #Create nested dictionaries for each character to map to each possible character
    states = {}
    for stateLetter in letters:
        evidence = {}
        for evidenceLetter in letters:
            #Initialize the stateCount to one
            #This is the numerator's part for smoothing
            evidence[evidenceLetter] = 1
        states[stateLetter] = evidence

    f = open(file, 'r')

    curr = f.readline()
    for line in f:
        states[curr[0]][line[0]] += 1
        curr = line

    #Run 27 times
    for stateKey, stateValue in states.iteritems():
        print 'P(Xt+1 | Xt)'

        #Run 27 times
        sumOfState = 0
        for evidenceKey, evidenceValue in stateValue.iteritems():
            prob = float(float(evidenceValue) / float((27 + stateSum(file, stateKey))))
            sumOfState += prob
            print 'P( X[' + evidenceKey + '] | X[' + stateKey + '] = ' + str(prob)

        #Always equals 1
        print 'Total probability of any evidence given state \'' + str(stateKey) + '\': '+ str(sumOfState) + '\n'
    print '----------end transition-------------------\n'

marginal('typos20.data')
emission('typos20.data')
transition('typos20.data')
