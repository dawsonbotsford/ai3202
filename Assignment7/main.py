probs = [0.82,  0.56, 0.08, 0.81, 0.34, 0.22, 0.37, 0.99, 0.55, 0.61, 0.31, 0.66, 0.28, 1.0,  0.95, 0.71, 0.14, 0.1,  1.0,  0.71, 0.1,  0.6,  0.64, 0.73, 0.39, 0.03, 0.99, 1.0,  0.97, 0.54, 0.8,  0.97, 0.07, 0.69, 0.43, 0.29, 0.61, 0.03, 0.13, 0.14, 0.13, 0.4,  0.94, 0.19, 0.6,  0.68, 0.36, 0.67, 0.12, 0.38, 0.42, 0.81, 0.0,  0.2,  0.85, 0.01, 0.55, 0.3,  0.3,  0.11, 0.83, 0.96, 0.41, 0.65, 0.29, 0.4,  0.54, 0.23, 0.74, 0.65, 0.38, 0.41, 0.82, 0.08, 0.39, 0.97, 0.95, 0.01, 0.62, 0.32, 0.56, 0.68, 0.32, 0.27, 0.77, 0.74, 0.79, 0.11, 0.29, 0.69, 0.99, 0.79, 0.21, 0.2,  0.43, 0.81, 0.9,  0.0,  0.91, 0.01]

class Node:
    def __init__(self, name, parents):
        self.name = name
        self.parents = []
        self.result = False #Boolean value if we know it's true
        self.chances = {} #The chance that this will be true given input key

    def setMarginal(self, value):
        self.marginal = value

    def getMarginal(self, value):
        return self.marginal

    def setResult(self, value): #Set boolean result
        self.result = value

    def getResult(self): #Get boolean result
        return self.result

    def setChances(self, key, value):
        self.chances[key] = value

    def getChances(self, key):
        return self.chances[key]

class Net:
    def __init__(self):
        self.cloudyNode = Node("cloudy", None)
        self.cloudyNode.setMarginal(0.5)

        self.sprinklerNode = Node("sprinkler", self.cloudyNode)
        self.sprinklerNode.setChances("c", 0.1)
        self.sprinklerNode.setChances("~c", 0.5)

        self.rainNode = Node("rainy", self.cloudyNode)
        self.rainNode.setChances("c", 0.8)
        self.rainNode.setChances("~c", 0.2)

        self.wetNode = Node("wet", [self.sprinklerNode, self.rainNode])
        self.wetNode.setChances("sr", 0.99)
        self.wetNode.setChances("s~r", 0.9)
        self.wetNode.setChances("~sr", 0.9)
        self.wetNode.setChances("~s~r", 0)
    def info(self):
        print "PRINTING INFO:"
        print "cloudy: " + str(self.cloudyNode.result)
        print "sprinkler: " + str(self.sprinklerNode.result)
        print "rain: " + str(self.rainNode.result)
        print "wet: " + str(self.wetNode.result)

    def prior(self, cloudy, sprinkler, rain, wet):
        # print "\ncloudy: " + str(cloudy)
        # print "chances: " + str(self.cloudyNode.marginal)
        if (self.cloudyNode.marginal > cloudy):
            self.cloudyNode.setResult(True)
            # print "Checking if " + str(self.sprinklerNode.getChances("c")) + " > " + str(sprinkler)
            if (self.sprinklerNode.getChances("c") > sprinkler):
                # print "!" + str(self.sprinklerNode.getChances("c")) + " > " + str(sprinkler)
                self.sprinklerNode.setResult(True)

            # print "Checking if " + str(self.rainNode.getChances("c")) + " > " + str(rain)
            if (self.rainNode.getChances("c") > rain):
                # print str(self.rainNode.getChances("c")) + " > " + str(rain)
                self.rainNode.setResult(True)

        else: #Not cloudy
            # print "Checking if " + str(self.sprinklerNode.getChances("~c")) + " > " + str(sprinkler)
            if (self.sprinklerNode.getChances("~c") > sprinkler):
                # print "!" + str(self.sprinklerNode.getChances("~c")) + " > " + str(sprinkler)
                self.sprinklerNode.setResult(True)

            if (self.rainNode.getChances("~c") > rain):
                # print str(self.rainNode.getChances("~c")) + " > " + str(rain)
                self.rainNode.setResult(True)

#If sprinkler and rain, check if it's wet
        if (self.sprinklerNode.result == True and self.rainNode.result == True):
            if (self.wetNode.getChances("sr") > wet):
                # print str(self.wetNode.getChances("sr")) + " > " + str(wet)
                # print "Setting rain node to true!"
                self.wetNode.setResult(True)

        elif (self.sprinklerNode.result == True and self.rainNode.result == False):
            if (self.wetNode.getChances("s~r") > wet):
                # print str(self.wetNode.getChances("sr")) + " > " + str(wet)
                # print "Setting rain node to true!"
                self.wetNode.setResult(True)

        elif (self.sprinklerNode.result == False and self.rainNode.result == True):
            if (self.wetNode.getChances("~sr") > wet):
                # print str(self.wetNode.getChances("~sr")) + " >= " + str(wet)
                # print "Setting rain node to true!"
                self.wetNode.setResult(True)

        #FALSE elif (self.sprinklerNode.result == False and self.rainNode.result == False):

def oneA():
    result = 0 #The final sum

    for i in range(25):
        net = Net()
        net.prior(probs[i*4], probs[i*4 + 1], probs[i*4 + 2], probs[i*4 + 3])
        if (net.cloudyNode.result == True):
            result += 1
    return str(result) + " / 25 or " + str(result * 4) + "%"


def oneB():
    result = 0 #The final sum

    #Save nets that have rain = true
    savedNets = []
    for i in range(25):
        net = Net()
        net.prior(probs[i*4], probs[i*4 + 1], probs[i*4 + 2], probs[i*4 + 3])
        if (net.rainNode.result == True):
            savedNets.append(net)

    for i in range(len(savedNets)):
        if (savedNets[i].cloudyNode.result == True):
            result += 1

    percent = result * 100.0 / len(savedNets)
    return str(result) + " / " + str(len(savedNets)) + " or " + str(percent) + "%"


#P(S|W)
def oneC():
    result = 0 #The final sum

    #Save nets that have wet = true
    savedNets = []
    for i in range(25):
        net = Net()
        net.prior(probs[i*4], probs[i*4 + 1], probs[i*4 + 2], probs[i*4 + 3])
        if (net.wetNode.result == True):
            savedNets.append(net)

    # print "len(savedNets): " + str(len(savedNets))
    for i in range(len(savedNets)):
        if (savedNets[i].sprinklerNode.result == True):
            result += 1

    if (len(savedNets) > 0):
        percent = result * 100.0 / len(savedNets)
    else:
        percent = 0

    return str(result) + " / " + str(len(savedNets)) + " or " + str(percent) + "%"

#P(S|C,W)
def oneD():

    #Save nets that have wet = true
    savedNets1 = []
    savedNets2 = []

    #Given W
    for i in range(25):
        net = Net()
        net.prior(probs[i*4], probs[i*4 + 1], probs[i*4 + 2], probs[i*4 + 3])
        if (net.wetNode.result == True):
            savedNets1.append(net)

    #Given C
    for i in range(len(savedNets1)):
        if (savedNets1[i].cloudyNode.result == True):
            savedNets2.append(savedNets1[i])

    result = 0 #The final sum
    for i in range(len(savedNets2)):
        if (savedNets2[i].sprinklerNode.result == True):
            result += 1

    if (len(savedNets2) > 0):
        percent = result * 100.0 / len(savedNets2)
    else:
        percent = 0

    return str(result) + " / " + str(len(savedNets2)) + " or " + str(percent) + "%"

def threeA():
    result = 0 #The final sum

    for i in range(100):
        net = Net()
        net.prior(probs[i], 1, 2, 3)
        if (net.cloudyNode.result == True):
            result += 1
    return str(result) + " / 100 or " + str(result) + "%"

#P(C|R)
#cloudy, sprinkler, rain, wet
def threeB():
    result = 0 #The final sum
    savedNets = []

    rainyCount = 0
    for i in range(50):
        net = Net()
        net.prior(probs[i*2], 1, probs[i*2+1], 3)
        if (net.rainNode.result == True):
            rainyCount += 1
            if (net.cloudyNode.result == True):
                result += 1
        else:
            #Halt because this is rejection
            continue

    percent = result * 100.0 / rainyCount

    return str(result) + " / " + str(rainyCount) + " or " + str(percent) + "%"

def threeC():
    return oneC()

def threeC():
    return oneC()

def threeD():
    return str(0)

#Calculate percent error for question 2 and 4
def percentError(experimental, theoretical):
    if theoretical != 0:
        # return str(abs((experimental - theoretical) / theoretical) * 100) + "%"
        return "Estimated: " + str(experimental) + "    Actual: " + str(theoretical) + "  % Err: " + str(abs((experimental - theoretical) / theoretical) * 100) + "%"
    else: 
        return "Actual: " + str(theoretical) + "     % Err: 0%"

print "\n1A result: " + oneA()
print "1B result: " + oneB()
print "1C result: " + oneC()
print "1D result: " + oneD()

print "\n2A Percent error: " + percentError(0.48, .5)
print "2B Percent error: " + percentError(0.75, .8)
print "2C Percent error: " + percentError(0.4, .71)
print "2D Percent error: " + percentError(0, 0)

print "\n3A result: " + threeA()
print "3B result: " + threeB()
#3c is done!
print "3C result: " + threeC()
print "3D result: " + threeD()

print "\n4A Percent error: " + percentError(0.49, .5)
print "4B Percent error: " + percentError(.70371, 0.8)
print "4C Percent error: " + percentError(0.4, .71)
print "4D Percent error: " + percentError(0, 0.1)
print ""
