#Assignment 7

##Output (Further explanation below)
```shell
1A result: 12 / 25 or 48%
1B result: 6 / 8 or 75.0%
1C result: 4 / 10 or 40.0%
1D result: 0 / 5 or 0.0%

2A Percent error: Estimated: 0.48    Actual: 0.5  % Err: 4.0%
2B Percent error: Estimated: 0.75    Actual: 0.8  % Err: 6.25%
2C Percent error: Estimated: 0.4    Actual: 0.71  % Err: 43.661971831%
2D Percent error: Actual: 0     % Err: 0%

3A result: 49 / 100 or 49%
3B result: 19 / 27 or 70.3703703704%
3C result: 4 / 10 or 40.0%
3D result: 0

4A Percent error: Estimated: 0.49    Actual: 0.5  % Err: 2.0%
4B Percent error: Estimated: 0.70371    Actual: 0.8  % Err: 12.03625%
4C Percent error: Estimated: 0.4    Actual: 0.71  % Err: 43.661971831%
4D Percent error: Estimated: 0    Actual: 0.1  % Err: 100.0%

```

<br>
##Runit
```shell
python main.py
```

<br>
####What is this?
Homework. ```open Assignment5.pdf``` for instructions

<br>
##Prior
1. Use Prior sampling to calculate
  <br>
  a. P(c=true) = 12 / 25 or **48%**
  <br>
  b. P(c=true | rain=true) = 6 / 8 or **75%**
  <br>
  c. P(s=true | w=true) = 4 / 10 or **40%**
  <br>
  d. P(s=true | c=true,w=true) = 0 / 5 or **0%**

---
<br>
2. Calculate the exact value for the probabilities in Problem 1 and comment on the error between the exact and approximate calculations.

  a. P(c=true) =  50% (Stated in the problem).
  That's an error of 4% as calculated in `main.py`. That's a small amount of error, so this was a **good** sample.
  <br>
  <br>
  b. P(c=true | rain=true)
  Before getting the % error, let's get the exact:
  ```
    P(c=true | rain=true) = P(c,r)/p(r)
    = (0.8)/(0.8 + 0.2) = 0.8
  ```
  That's an error of 6.25% as calculated in `main.py`. That's a small amount of error, so this was a **good** sample.
  <br>
  <br>
  c. P(s=true | w=true)
  Before getting the % error, let's get the exact:
  ```
    P(s=true | w=true) = P(s,w)/p(w)
    = (0.10 + 0.99 + 0.9) / (0.99 + 0.9 + 0.9) = 0.71
  ```
  That's an error of 77.5% as calculated in `main.py`. That's a large error, so this was a **bad** sample. This is likely because of how small of a sample size this results in.
  <br>
  <br>
  d. See calculation in code == 0.1
  <br>This is a small error even if it seems large. It's an issue with 0 and the error formula
 
---
<br>
##Rejection
3. Use Rejection sampling to calculate the following probabilities. Use all 100 samples for each query.
    <br>
    a. P(c=true) = 49 / 100 or **49%**
    <br>
    b. P(c=true | rain=true) = 19 / 27 or **70.37%**
    <br>
    c. P(s=true | w=true) = 4 / 10 or **40%**
    <br>
    d. P(s=true | c=true,w=true) = **0%**

---
<br>
4. Did Rejection sampling produce the same results as Prior sampling for each of the probabilities? If not, comment on the error rates for each of the calculations.

  a. P(c=true) =  50% (Stated in the problem).
  That's an error of 2% as calculated in `main.py`. That's a small amount of error, so this was a **good** sample.
  <br>
  <br>
  b. P(c=true | rain=true)
  Exact = 0.8 (calculated in 2b)
  That's an error of ____% as calculated in `main.py`. That's a small amount of error, so this was a **good** sample.
  <br>
  <br>
  c. P(s=true | w=true)
  Exact = 0.71 (calculated in 2c)
  That's an error of 77.5% as calculated in `main.py`. That's a large error, so this was a **bad** sample. This is likely because of how small of a sample size this results in.
  <br>
  <br>
  d. P(s=true | c=true,w=true)


